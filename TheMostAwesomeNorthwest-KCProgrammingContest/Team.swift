//
//  Team.swift
//  TheMostAwesomeNorthwest-KCProgrammingContest
//
//  Created by Vincent Dong on 3/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import Foundation

struct Team : CustomStringConvertible, Equatable {
    var teamName:String
    var student0:String
    var student1:String
    var student2:String
    
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.teamName == rhs.teamName
    }
    
    var description: String {
        return "\(teamName)"
    }
}

struct Teams {
    
    static var shared = Teams()
    
    private var teams:[Team] = [
        Team(teamName: "Tonka 0", student0: "Dagget", student1: "Rogers", student2: "Vaughan"),
        Team(teamName: "Tonka 1", student0: "", student1: "", student2: ""),
        ]
    
    private init(){}
    
    func numTeams()->Int {
        return teams.count
    }
    
    func team(_ index:Int) -> Team {
        return teams[index]
    }
    
    subscript(index:Int) -> Team {
        return teams[index]
    }
    
    mutating func add(team:Team){
        teams.append(team)
    }
    
    mutating func delete(team:Team){
        for i in 0 ..< teams.count {
            if teams[i] == team {
                teams.remove(at:i)
                break
            }
        }
        
    }
}
