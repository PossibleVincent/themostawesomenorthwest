//
//  StudentsViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContest
//
//  Created by Vincent Dong on 3/12/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {

    @IBOutlet weak var student0: UILabel!
    
    @IBOutlet weak var student1: UILabel!
    
    @IBOutlet weak var student2: UILabel!
    
    var team:Team!
    
    override func viewWillAppear(_ animated: Bool) {
        student0.text = "\(team.student0)"
        student1.text = "\(team.student1)"
        student2.text = "\(team.student2)"
        navigationItem.title = "\(team.teamName)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
