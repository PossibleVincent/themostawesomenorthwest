//
//  NewSchoolViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContest
//
//  Created by Vincent Dong on 3/12/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
    
    var newSchool:School!

    @IBOutlet weak var newSchoolName: UITextField!
    
    @IBOutlet weak var newCoach: UITextField!
    
    @IBAction func newSchoolDone(_ sender: Any) {
        let name = newSchoolName.text!
        let coach = newCoach.text!
        
        Schools.shared.add(school: School(name:name, coach:coach))
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func newSchoolCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
