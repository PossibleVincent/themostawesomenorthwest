//
//  NewTeamViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContest
//
//  Created by Vincent Dong on 3/12/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
    
    var newTeam:Team!

    @IBOutlet weak var newTeamName: UITextField!
    
    @IBOutlet weak var newStudent0: UITextField!
    
    @IBOutlet weak var newStudent1: UITextField!
    
    @IBOutlet weak var newStudent2: UITextField!
    
    @IBAction func newTeamDone(_ sender: Any) {
        let teamName = newTeamName.text!
        let student0 = newStudent0.text!
        let student1 = newStudent1.text!
        let student2 = newStudent2.text!
        
        Teams.shared.add(team: Team(teamName:teamName, student0:student0, student1:student1, student2:student2))
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func newTeamCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
