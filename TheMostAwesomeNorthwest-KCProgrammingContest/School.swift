//
//  School.swift
//  TheMostAwesomeNorthwest-KCProgrammingContest
//
//  Created by Vincent Dong on 3/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import Foundation

struct School : CustomStringConvertible, Equatable {
    var name:String
    var coach:String
    
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name
    }
    
    var description: String {
        return "\(name) -- \(coach)"
    }
}

struct Schools {
    
    static var shared = Schools()
    
    private var schools:[School] = [
        School(name: "Platte County High School", coach: "Sarah Jones"),
        School(name: "Ozark High School", coach: "Steve Anderson"),
        School(name: "Winnetonka High School", coach: "Bradley Meeks"),
        ]
    
    private init(){}
    
    func numSchools()->Int {
        return schools.count
    }
    
    func school(_ index:Int) -> School {
        return schools[index]
    }
    
    subscript(index:Int) -> School {
        return schools[index]
    }
    
    mutating func add(school:School){
        schools.append(school)
    }
    
    mutating func delete(school:School){
        for i in 0 ..< schools.count {
            if schools[i] == school {
                schools.remove(at:i)
                break
            }
        }
        
    }
}
